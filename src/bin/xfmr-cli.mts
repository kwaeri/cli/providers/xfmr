#!/usr/bin/env node

/*******************************************************************************
 * @module  kwaeri/xfmr
 * @version 0.4.0
 * @license
 *  Copyright © 2014 - 2022 Richard Winters <kirvedx@gmail.com> and contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 * SPDX-License-Identifier: Apache-2.0
 ******************************************************************************/


'use strict'


// INCLUDES
//import * as process from 'process';
import { Progress } from '@kwaeri/progress';
import { ServiceEventBits } from '@kwaeri/service';
import { Transformer } from '../xfmr.mjs';
import { kdt } from '@kwaeri/developer-tools';
import debug from 'debug';
import EventEmitter from 'events';


// GLOBALS
const DEBUG = debug( 'kue:xfmr-cli' );
const _         = new kdt(),
      //emitter   = new EventEmitter(),
      progress  = new Progress({ spinner: true, spinAnim: "dots", percentage: false }),
      xfmr      = new Transformer( progress.getHandler() );

progress.init();

//emitter.on(
//    "ServiceEvent",
//    progress.handler.bind( progress )// serviceEventHandler
//);

// PROCEDURE
DEBUG( `Parse command line arguments` );

const args = // Allows us to accept arguments to our gulpfile
(
    argList => {

        let argMap: { [key: string]: string | boolean } = {},
            i,
            option, thisOption, currentOption;

        for( i = 0; i < argList.length; i++ ) {
            thisOption = argList[i].trim();

            option = thisOption.replace( /^\-+/, '' );

            if( option === thisOption ) {
                // argument value
                if( currentOption )
                    argMap[currentOption] = option;

                currentOption = null;
            }
            else {
                currentOption = option;
                argMap[currentOption] = true;
            }
        }

        return argMap;
    }
)( process.argv );


DEBUG( `Set constants` );
const BUMP_VERSION          = args['bump-version'],
      BUMP_COPYRIGHT        = args['bump-copyright'],
      SOURCE_VERSION        = args['source'],
      PROJECT_VERSION       = args["project"],
      TEST                  = args['test'],
      COMPONENT             = args['component'],
      TO_VERSION            = args['to-version'];

const stdinListener = ( data: any ) => console.log( `Received: ${data.toString( 'utf8' )}` );


DEBUG( `Testing STDIN` );
process.stdin.on(
    "data",
    stdinListener
);


DEBUG( `Invoke Transformer` );
try {
    xfmr.updateProgress( "XFMR", { progressLevel: 0, notice: "Transforming target sources..." } );

    const transformed = await xfmr.renderService(
        {
            quest: "bump",
            specification: ( BUMP_VERSION ) ? "version" : ( BUMP_COPYRIGHT ) ? "copyright" : 'version',
            subCommands: [],
            args: {
                project: PROJECT_VERSION,
                source: SOURCE_VERSION,
                component: COMPONENT,
                "to-version": TO_VERSION,
                test: TEST
            },
            version: ""
        }
    );

    if( transformed && transformed.result ) {

        xfmr.updateProgress(
            "XFMR",
            {
                progressLevel: 100,
                notice: `Finished ${BUMP_VERSION ? `version` : BUMP_COPYRIGHT ? `copyright` : `version`} transformation.`
            }
        );

        xfmr.updateProgress( "XFMR", { progressLevel: -1 } );

        /*console.log(
            `Finished ${BUMP_VERSION ? `version` : BUMP_COPYRIGHT ? `copyright` : `version`} transformation\n\n`,
            `⇨ File(s):\n`,
            transformed.targets
        );
        */
    }
    else {
        console.log( transformed );
    }
    //emitter.removeListener( "ServiceEvent", serviceEventHandler );

    // To properly exit, removing the listener doesn't work:
    //process.stdin.removeListener( "data", stdinListener );
    // Instead, we need to unreference the process.stdin stream from this script:
    process.stdin.unref();

}
catch( error ) {
    console.error( error );


    xfmr.updateProgress(
        "XFMR",
        {
            log: `Error: ${( error as any ).name}: ${( error as any ).message}}`
        }
    );

    xfmr.updateProgress( "XFMR", { progressLevel: -1 } );
    //emitter.removeListener( "ServiceEvent", serviceEventHandler );

    process.stdin.unref();
}
