/*******************************************************************************
 * @module  kwaeri/xfmr
 * @version 0.4.0
 * @license
 *  Copyright © 2014 - 2022 Richard Winters <kirvedx@gmail.com> and contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 * SPDX-License-Identifier: Apache-2.0
 ******************************************************************************/


 'use strict'


// INCLUDES
import * as assert from 'assert';
import { Transformer } from '../src/xfmr.mjs';


// DEFINES
const esp = new Transformer();

// SANITY CHECK - Makes sure our tests are working proerly
describe(
    'PREREQUISITE',
    () => {
        describe(
            'Sanity Test(s)',
            () => {

                it(
                    'Should return true.',
                    () => {
                        assert.equal( [1,2,3,4].indexOf(4), 3 );
                    }
                );

            }
        );
    }
);


describe(
    'XFMR Functionality Test Suite',
    () => {

        describe(
            'Get Service Type Test',
            () => {
                it(
                    'Should return true, having matched the expected service type.',
                    async () => {
                        const testMethodResult = esp.serviceType;

                        return Promise.resolve(
                            assert.equal( testMethodResult, "Standard Service" )
                        );
                    }
                );
            }
        );

        describe(
            'Get Service Provider Subscriptions Test',
            () => {
                it(
                    'Should return true, having matched the expected stringified contracts.',
                    async () => {
                        const testMethodResult = esp.getServiceProviderSubscriptions();

                        return Promise.resolve(
                            assert.equal(
                                JSON.stringify( testMethodResult ),
                                JSON.stringify(
                                    {
                                        commands: {
                                            "bump": {
                                                "version": false,
                                                "copyright": false
                                            }
                                        },
                                        required: {
                                            "bump": {
                                                "version": {
                                                },
                                                "copyright": {
                                                }
                                            }
                                        },
                                        optional: {
                                            "bump": {
                                                "version": {
                                                    "component": {
                                                        "for": false,
                                                        "flag": false,
                                                        "values": [
                                                            "major",
                                                            "minor"
                                                        ]
                                                    },
                                                    "to-version": {
                                                        "for": false,
                                                        "flag": false,
                                                        "values": [ "*" ]
                                                    },
                                                    "project": {
                                                        "for": false,
                                                        "flag": true
                                                    },
                                                    "source": {
                                                        "for": false,
                                                        "flag": true
                                                    }
                                                },
                                                "copyright": {
                                                    "type": {
                                                        "for": false,
                                                        "flag": false,
                                                        "values": [
                                                            "range",
                                                            "each",
                                                            "current"
                                                        ]
                                                    }
                                                }
                                            }
                                        }
                                    }
                                )
                            )
                        );
                    }
                );
            }
        );

        describe(
            'Get Service Service Provider Subscription Help Text Test',
            () => {
                it(
                    'Should return true, having matched the expected stringified help text.',
                    async () => {
                        const testMethodResult = esp.getServiceProviderSubscriptionHelpText();

                        return Promise.resolve(
                            assert.equal(
                                JSON.stringify( testMethodResult ),
                                JSON.stringify(
                                    {
                                        "helpText": {
                                            "commands": {
                                                "bump": {
                                                    "description": "The 'bump' command automates semver transformation.",
                                                    "specifications": {
                                                        "version": {
                                                            "description": "Bumps the semver string within source files of the project, and according to options provided.",
                                                            "options": {
                                                                "required": {
                                                                },
                                                                "optional": {
                                                                    "specification": {
                                                                        "component": {
                                                                            "description": "Denotes that the version string should be bumped based on a component.",
                                                                            "values": [
                                                                                "major",
                                                                                "minor"
                                                                            ]
                                                                        },
                                                                        "to-version": {
                                                                            "description": "Denotes that the version string should be set to a specific version.",
                                                                            "values": [
                                                                                "Any semantic version (i.e. x.x.x)",
                                                                            ]
                                                                        },
                                                                        "project": {
                                                                            "description": "Denotes that the version should be bumped within the project's `package.json` file by leveraging it's alternate key.",
                                                                            "values": false
                                                                        },
                                                                        "source": {
                                                                            "description": "Denotes that the version should be bumped within project source files.",
                                                                            "values": false
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        },
                                                        "copyright": {
                                                            "description": "Bumps the copyright string within source files of the project, and according to options provided.",
                                                            "options": {
                                                                "required": {
                                                                },
                                                                "optional": {
                                                                    "specification": {
                                                                        "type": {
                                                                            "description": "Denotes that the copyright string is of the type specified.",
                                                                            "values": [
                                                                                "range",
                                                                                "each",
                                                                                "current"
                                                                            ]
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    },
                                                    "options": {
                                                        "optional": {
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                )
                            )
                        );
                    }
                );
            }
        );

        describe(
            'Render Service Test',
            () => {
                it(
                    'Should return true, having successfully transformed the version string within the file(s).',
                    async () => {
                        const testMethodResult = await esp.renderService( { quest: "bump", specification: "version", subCommands: [], args: { project: true, test: true }, version: "", configuration: { project: { name: "", type: "", tech: "", repository: "", author: { fullName: "", first: "", last: "", email: "" }, copyright: "", copyrightEmail: "", license: { identifier: '' }, root: "" } } } );

                        return Promise.resolve(
                            assert.equal(
                                JSON.stringify( testMethodResult ),
                                JSON.stringify(
                                    {
                                        result: true,
                                        type: "bump_version",
                                        targets: [
                                            "package.json"
                                        ]
                                    }
                                )
                            )
                        );
                    }
                );
            }
        );

    }
);